#Function
Add traffic rank to whois records

#Usage
1.
python3 gen.py -i <alexa_top_1m_domains> -w <whois file> -t <num_of_threads> (optional: -o <output dir>)  
e.g.  
python gen.py -i top-1m.csv -w alexa/full -t 10  
2. Use default settings in config.py  
python gen.py -i <top_1m filename in default path> -w <yyyy_mm_dd:mode{full, regular, simple}> -t <num_of_threads> (optional: -o <output dir>)  
e.g.  
python3 gen.py --useconfig -i May_23.zip -w 2019_03_29:simple -t 10 -o out/regular

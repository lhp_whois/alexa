import zipfile
import os
import shutil
import csv
from config import TMP_DIR

def extractZip(zip_file):
    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR)
    os.makedirs(TMP_DIR)
    zipReader = zipfile.ZipFile(zip_file, 'r')
    zipReader.extractall(TMP_DIR)
    return os.path.join(TMP_DIR, os.listdir(TMP_DIR)[0])

def collectWhoisFiles(whoisf):
    whoisfiles = []
    if os.path.isdir(whoisf):
        filenames = os.listdir(whoisf)
        filenames = list(filter(lambda x:x.endswith('.csv'), filenames))
        for filename in filenames:
            whoisfiles.append(os.path.join(whoisf, filename))
    else:
        whoisfiles.append(whoisf)
    return whoisfiles

def loadTop1M(inputf):
    traffic_rank = {}
    with open(inputf, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            traffic_rank[row[1]] = row[0]
    return traffic_rank

#!/usr/bin/env python3
import csv
import os
import multiprocessing
from argparse import ArgumentParser
from config import OUTPUT_DIR, WHOIS_PATH, TOP_1M_PATH
from lib import extractZip, collectWhoisFiles, loadTop1M

argparser = ArgumentParser(description='add traffic rank to whois records')
argparser.add_argument('-i', '--input', help='input file path from alexa', required=True)
argparser.add_argument('-w', '--whois', help='whois records file path', required=True)
argparser.add_argument('-o', '--output', help='output file, if not assigned, use the OUTPUT_DIR in config.py')
argparser.add_argument('-t', '--threads', help='threads, default 1', type=int, default=1)
argparser.add_argument('--useconfig', action='store_true')

args = argparser.parse_args()

inputf = ''
whoisf = ''
out_dir = OUTPUT_DIR
if args.useconfig:
    inputf = TOP_1M_PATH.format(filename=args.input)
    date, mode = args.whois.split(':')
    whoisf = WHOIS_PATH.format(date=date, mode=mode)
else:
    inputf = args.input
    whoisf = args.whois

if args.output:
    out_dir = args.output

threads = args.threads

if not os.path.exists(out_dir):
    os.makedirs(out_dir)

inputf = extractZip(inputf)
traffic_rank = loadTop1M(inputf)
whoisfiles = collectWhoisFiles(whoisf)

def add_traffic_to_file(file):
    filename = os.path.basename(file)
    with open(file, 'r') as f:
        reader = csv.reader(f)
        header = next(reader)
        outf = os.path.join(out_dir, filename)
        with open(outf, 'w') as wf:
            writer = csv.writer(wf)
            header.insert(0, 'traffic_rank')
            writer.writerow(header)
            for row in reader:
                row.insert(0, traffic_rank.get(row[0],''))
                writer.writerow(row)

with multiprocessing.Pool(threads) as w_pool:
    w_pool.map(add_traffic_to_file, whoisfiles)
